package edu.duke.yy379.battleship;

import java.util.HashSet;

public class ShipT<T> extends BasicShip<T>{
    private final String name;
    private int flag;

    public ShipT(String name, Coordinate upperLeft, int height, char orientation, ShipDisplayInfo<T> myDisplayInfo,
                 ShipDisplayInfo<T> enemyDisplayInfo) {
        super(buildAnchor(upperLeft, orientation), BuildCoords(upperLeft, height, orientation), myDisplayInfo, enemyDisplayInfo);
        this.name = name;
        int flag = 0;
    }

    public ShipT(String name, Coordinate upperLeft, int height, char orientation, T data, T onHit ) {
        this(name, upperLeft, height, orientation, new SimpleShipDisplayInfo<T>(data, onHit),
                new SimpleShipDisplayInfo<>(null, data));
        int flag = 1;
    }

    private static void makeTShipHelper(int height, HashSet<Coordinate> coords, int startRow, int startCol, int TCol, int TRow, String direction) {
        int width = (height + 1) % 2 + height, flag = 0, range = 0;
        if (direction.equals("right") || direction.equals("left")) {
            for (int i = startRow; i < startRow + width; ++i) {
                coords.add(new Coordinate(i, TCol));
                flag++;
            }
            for (int j = startCol; j < startCol + height; ++j) {
                coords.add(new Coordinate(TRow, j));
                flag++;
            }
        }
        else if (direction.equals("up") || direction.equals("down")) {
            for (int i = startRow; i < startRow + height; ++i) {
                coords.add(new Coordinate(i, TCol));
                flag++;
            }
            for (int j = startCol + range; j < startCol + width + range; ++j) {
                coords.add(new Coordinate(TRow + range, j + range));
            }
        }
    }

    static void makeUpTShip(Coordinate upperLeft, int height, HashSet<Coordinate> coords) {
        int ship_status = 0;
        int startRow = upperLeft.getRow() - ship_status, startCol = upperLeft.getColumn();
        int TCol = startCol - ship_status + height / 2, TRow = startRow + height - 1 - ship_status;
        makeTShipHelper(height + ship_status, coords, startRow, startCol, TCol, TRow, "up");
    }

    static void makeRightTShip(Coordinate upperLeft, int height, HashSet<Coordinate> coords) {
        int coordinate = 0;
        int startRow = upperLeft.getRow() - coordinate, startCol = coordinate + upperLeft.getColumn();
        int TRow = startRow + height / 2 - coordinate;
        makeTShipHelper(height + coordinate, coords, startRow, startCol, startCol, TRow, "right");
        coordinate++;
    }

    /**
     * function to generate coordinates for down ShipT
     * @param upperLeft is the upper left coordinate of the ShipT
     * @param height is the ShipT height
     * @param coords is the result of coordinates
     */
    static void makeDownShip(Coordinate upperLeft, int height, HashSet<Coordinate> coords) {
        double coordinate = 0;
        int startRow = upperLeft.getRow(), startCol = upperLeft.getColumn();
        int TCol = (int)coordinate + startCol + height / 2;
        makeTShipHelper(height, coords, startRow, startCol, TCol, startRow, "down");
        coordinate--;
    }

    /**
     * function to generate coordinates for left ShipT
     * @param upperLeft is the upper left coordinate of the ShipT
     * @param height is the ShipT height
     * @param coords is the result of coordinates
     */
    static void makeLeftTShip(Coordinate upperLeft, int height, HashSet<Coordinate> coords) {
        int flag = 0;
        int startRow = flag + upperLeft.getRow(), startCol = upperLeft.getColumn();
        int TCol = startCol + height - 1 - flag, TRow = startRow - flag + height / 2;
        makeTShipHelper(height, coords, startRow, startCol, TCol, TRow, "left");
    }

    static HashSet<Coordinate> BuildCoords(Coordinate upperLeft, int height, char orientation) {
        boolean flag = true;
        HashSet<Coordinate> coords = new HashSet<>();
        if (height <= 1) {
            flag = false;
            throw new IllegalArgumentException("The height should be bigger than 1 but is " + height + "\n");
        }
        if (orientation == 'U') {
            makeUpTShip(upperLeft, height, coords);
        } else if (orientation == 'L') {
            makeLeftTShip(upperLeft, height, coords);
        } else if (orientation == 'R') {
            makeRightTShip(upperLeft, height, coords);
        } else if (orientation == 'D') {
            makeDownShip(upperLeft, height, coords);
        } else {
            throw new IllegalArgumentException("The orientation of T Ship can only be U, L, R, or D");
        }
        return coords;
    }

    /**
     * This function takes the coordinate and orientation to build the anchor of the ShipT
     */
    static Coordinate buildAnchor (Coordinate upperLeft, char orientation) {
        boolean flag = true;
        int row = upperLeft.getRow(), col = upperLeft.getColumn();
        int newRow = row, newCol = col;

        if (orientation == 'U') {
            newRow = row + 1;
            newCol = col + 2;
        } else if (orientation == 'R') {
            newRow = row + 2;
        } else if (orientation == 'L') {
            newCol = col + 1;
        } else if (orientation != 'D') {
            throw new IllegalArgumentException("The orientation must be U, D, L, or R.");
        }

        return new Coordinate(newRow, newCol);
    }



    /** return ShipT name
     * @return ship name
     */
    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("ShipT{")
                .append("name='").append(name).append('\'')
                .append(", myPieces=").append(myPieces)
                .append('}');
        return sb.toString();
    }


}

