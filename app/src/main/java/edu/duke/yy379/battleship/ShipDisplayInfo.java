package edu.duke.yy379.battleship;

/**
 * This interface represents an Abstract Factory pattern for Ship creation.
 */
public interface ShipDisplayInfo<T> {
    public T getInfo(Coordinate where, boolean hit);
}

