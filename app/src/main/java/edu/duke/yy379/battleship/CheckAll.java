package edu.duke.yy379.battleship;

public class CheckAll<T> extends CheckOne<T> {

    public CheckAll(CheckOne<T> next) {
        super(next);
    }
    public boolean win = true;
    public boolean f = false;
    /**
     * @param theBoard
     * @return
     */
    @Override
    protected boolean checkMyRule(Board<T> theBoard) {
        for (Ship<T> ship : theBoard.getShips()) {
            if (!ship.isSunk() && win) {
                return f;
            }
        }
        return win;
    }
}


