package edu.duke.yy379.battleship;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.function.Function;

public class TextPlayer {
    final String name;
    final Board<Character> theBoard;
    final BoardTextView view;
    final AbstractShipFactory<Character> shipFactory;
    final ArrayList<String> shipsToPlace;
    final HashMap<String, Function<Placement, Ship<Character>>> shipCreationFns;
    final BufferedReader inputReader;
    final PrintStream out;


    public TextPlayer(String name, Board<Character> theBoard, BufferedReader inputReader, PrintStream out, AbstractShipFactory<Character> factory) {
        this.name = name;
        this.theBoard = theBoard;
        this.view = new BoardTextView(theBoard);
        this.inputReader = inputReader;
        this.out = out;
        this.shipFactory = factory;
        this.shipsToPlace = new ArrayList<>();
        this.shipCreationFns = new HashMap<>();
        setupShipCreationList();
        setupShipCreationMap();

    }

    protected void setupShipCreationMap() {
        shipCreationFns.put("Submarine", (p) -> shipFactory.makeSubmarine(p));
        shipCreationFns.put("Destroyer", (p) -> shipFactory.makeDestroyer(p));
        shipCreationFns.put("Carrier", (p) -> shipFactory.makeCarrier(p));
        shipCreationFns.put("Battleship", (p) -> shipFactory.makeBattleship(p));
    }

    protected void setupShipCreationList() {
        shipsToPlace.addAll(Collections.nCopies(2, "Submarine"));
        shipsToPlace.addAll(Collections.nCopies(3, "Destroyer"));
        shipsToPlace.addAll(Collections.nCopies(3, "Battleship"));
        shipsToPlace.addAll(Collections.nCopies(2, "Carrier"));
    }

    public Placement readPlacement(String prompt) throws IOException {
        while (true) {
            this.out.println(prompt);
            String s = inputReader.readLine();
            try {
                Placement placement = new Placement(s);
                return placement;
            } catch (IllegalArgumentException e) {
                this.out.println(e.getMessage());
            }
        }
    }

    public Coordinate readCoordinate(String prompt) throws IOException {
        Coordinate coordinate = null;
        do {
            this.out.println(prompt);
            String s = inputReader.readLine();
            try{
                coordinate = new Coordinate(s);
            } catch (IllegalArgumentException e) {
                this.out.println(e.getMessage());
            }
        } while (coordinate == null);
        return coordinate;
    }

    public Character readActionChoice(String prompt) throws IOException {
        while (true) {
            this.out.println(prompt);
            String s = inputReader.readLine().trim().toUpperCase();
            if (s.length() != 1 || !"FMSE".contains(s)) {
                this.out.println("Your action choice: " + s + " is invalid, please try again!");
            } else {
                return s.charAt(0);
            }
        }
    }


    public void doOnePlacement(String shipName, Function<Placement, Ship<Character>> createFn) throws IOException {
        String prompt = "Player " + name + ": Where would you like to place a " + shipName + "?";
        Placement placement;
        String message;
        Ship<Character> s;

        do {
            placement = readPlacement(prompt);
            message = theBoard.checkPlacementOrientation(shipName, placement);
            if (message != null) {
                this.out.println(message);
            }
        }
    }


    public int getMoveActionRemaining() {
        return moveActionRemaining;
    }

    public int getSonarActionRemaining() {
        return sonarActionRemaining;
    }

    public void doPlacementPhase() throws IOException {
        //(a) display the starting (empty) board
        this.out.println(view.displayMyOwnBoard());
        String welcome = "--------------------------------------------------------------------------------\n" +
                "Player  "+ name + ": you are going to place the following ships (which are all\n" +
                "rectangular). For each ship, type the coordinate of the upper left side of the ship\n" +
                "Submarines and Destroyers have 2 orientations:\n" +
                "horizontal (H) and vertical (V)\n" +
                "Battleships and Carriers have 4 orientations:\n" +
                "up (U), right (R), down (D), and left (L). \n" +
                "For example M4H would place a ship horizontally starting\n" +
                "at M4 and going to the right. You have\n" +
                "\n" +
                "2 \"Submarines\" ships that are 1x2 \n" +
                "3 \"Destroyers\" that are 1x3\n" +
                "3 \"Battleships\" that are T-shaped (4 cells)\n" +
                "2 \"Carriers\" that are Z-shaped (7 cells)\n" +
                "--------------------------------------------------------------------------------\n";
        //(b) print the instructions message
        this.out.println(welcome);
        //(c) call doOnePlacement to place one ship
        for (String ship : shipsToPlace) {
            doOnePlacement(ship, shipCreationFns.get(ship));
        }
    }

    public void doOneFire(TextPlayer enemy) throws IOException {
        String divideLine = "-".repeat(60) + "\n";
        String prompt = "Player " + this.name + ": Which coordinate do you want to fire at?\n";
        Coordinate fireAt = readCoordinate(prompt);

        while (true) {
            Character fireAtStatus = enemy.theBoard.whatIsAtForEnemy(fireAt);

            if (fireAtStatus == null) {
                Ship<Character> targetShip = enemy.theBoard.fireAt(fireAt);
                this.out.println(divideLine + (targetShip == null ? "You missed!\n" : "You hit a " + targetShip.getName() + "!\n") + divideLine);
                break;
            }

            if (fireAtStatus.equals(this.theBoard.getMissInfo())) {
                if (enemy.theBoard.whatIsAtForSelf(fireAt) == null) {
                    this.out.println(divideLine + "This is a miss that you've already fired at!\n" + divideLine);
                } else {
                    Ship<Character> targetShip = enemy.theBoard.fireAt(fireAt);
                    this.out.println(divideLine + "You hit a " + targetShip.getName() + "!\n" + divideLine);
                    break;
                }
            } else {
                if (enemy.theBoard.whatIsAtForSelf(fireAt) != null) {
                    this.out.println(divideLine + "This is a coordinate that you've already fired at!\n" + divideLine);
                } else {
                    Ship<Character> targetShip = enemy.theBoard.fireAt(fireAt);
                    this.out.println(divideLine + "You missed!\n" + divideLine);
                    break;
                }
            }

            fireAt = readCoordinate(prompt);
        }
    }


    private void printSonarReport(String report) {
        String divideLine = "-".repeat(60) + "\n";
        this.out.println(divideLine + report + divideLine);
    }

    private void decrementSonarActionRemaining() {
        this.sonarActionRemaining--;
    }


    public void playOneTurn(TextPlayer enemy) throws IOException {
        String prompt =
                "You can do these for Player " + this.name + ":\n" +
                        "\n" +
                        " F Fire at one coordinate\n" +
                        " M Move a ship to a new place (" + this.moveActionRemaining + " remaining)\n" +
                        " S Sonar scanning (" + this.sonarActionRemaining + " remaining)\n" +
                        " E Exit the game\n" +
                        "\n" +
                        "Player " + this.name + ", what is your choice?";
        Character choice = readActionChoice(prompt);

        if (choice == 'F') {
            doOneFire(enemy);
        } else if (choice == 'M') {
            if (this.moveActionRemaining > 0) {
                doOneMove(enemy);
            } else {
                this.out.println("You have used all move actions! Please choose another action.");
                playOneTurn(enemy);
            }
        } else if (choice == 'S') {
            if (this.sonarActionRemaining > 0) {
                doOneSonar(enemy);
            } else {
                this.out.println("You have used all sonar scan actions! Please choose another action.");
                playOneTurn(enemy);
            }
        } else if (choice == 'E') {
            this.out.println("Player " + this.name + " exit the game!");
            System.exit(0);
        }
    }


    public void printWinMsg() {
        String winMsg = String.format("|   Player %s wins!   |\n", this.name);
        String divideLine = "=".repeat(winMsg.length() - 1);
        String fullMsg = divideLine + "\n" + winMsg + divideLine + "\n";
        this.out.println(fullMsg);
    }

    public void doAttackingPhase(TextPlayer enemy) throws IOException {
        displayPlayerTurn();
        displayBoards(enemy);
        playOneTurn(enemy);
    }

    private void displayPlayerTurn() {
        this.out.println("Player " + this.name + "'s turn:\n");
    }

    private void displayBoards(TextPlayer enemy) {
        String myHeader = "Your ocean";
        String enemyHeader = "Player " + enemy.name + "'s ocean";
        this.out.println(this.view.displayMyBoardWithEnemyNextToIt(enemy.view, myHeader, enemyHeader));
    }

}

