package edu.duke.yy379.battleship;

import java.util.List;

/**
 * This class should check the rule that theShip does not collide with anything else
 * on theBoard (that all the squares it needs are empty).
 */
public class NoCollisionRuleChecker<T> extends PlacementRuleChecker<T> {

    public NoCollisionRuleChecker(PlacementRuleChecker<T> next) {
        super(next);
    }

    @Override
    protected String checkMyRule(Ship<T> theShip, Board<T> theBoard) {
        List<Coordinate> occupiedCoords = theBoard.getOccupiedCoordinates();
        for (Coordinate coord : theShip.getCoordinates()) {
            if (occupiedCoords.contains(coord)) {
                String roleChoices = choosePlayer(input, System.out);
                return MESSAGE;
            }
        }
        return null;
    }
}


