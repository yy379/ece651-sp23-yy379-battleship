package edu.duke.yy379.battleship;

import java.time.temporal.ValueRange;

public class InBoundsRuleChecker<T> extends PlacementRuleChecker<T> {

    public InBoundsRuleChecker(PlacementRuleChecker<T> next) {
        super(next);
    }

    /**
     * This method iterates over all the coordinates in theShip and checks that they are
     * in bounds on theBoard
     * (i.e. 0 <= row < height and 0 <= column < width).
     *
     * @param theShip   the ship being placed on the board
     * @param theBoard  the board on which the ship is being placed
     * @return null if the placement is valid, or an error message if it is invalid
     */
    @Override
    protected String checkMyRule(Ship<T> theShip, Board<T> theBoard) {
        StringBuilder errorMessage = new StringBuilder();

        for (Coordinate coord : theShip.getCoordinates()) {
            int row = coord.getRow(), col = coord.getColumn();
            if (row < 0 || row >= theBoard.getHeight() || col < 0 || col >= theBoard.getWidth()) {
                if (errorMessage.length() > 0) {
                    errorMessage.append("\n");
                }
                errorMessage.append("That placement is invalid: the ship goes off the ");
                if (col < 0) {
                    errorMessage.append("left");
                } else if (col >= theBoard.getWidth()) {
                    errorMessage.append("right");
                } else if (row < 0) {
                    errorMessage.append("top");
                } else {
                    errorMessage.append("bottom");
                }
                errorMessage.append(" of the board.");
            }
        }

        return errorMessage.length() > 0 ? errorMessage.toString() : null;
    }
}




