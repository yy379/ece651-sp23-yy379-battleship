package edu.duke.yy379.battleship;

public class SimpleShipDisplayInfo<T> implements ShipDisplayInfo<T> {
    //have two fields of type T: one for myData and one for onHit
    private T myData;
    private T onHit;
    //have a constructor that takes two Ts and initializes myData and onHit
    public SimpleShipDisplayInfo(T myData, T onHit) {
        this.myData = myData;
        this.onHit = onHit;
    }
    //getInfo check if (hit) and returns onHit if so, and myData otherwise.
    @Override
    public T getInfo(Coordinate where, boolean hit) {
        if (hit) {
            return onHit;
        }
        else {
            return myData;
        }
    }
}
