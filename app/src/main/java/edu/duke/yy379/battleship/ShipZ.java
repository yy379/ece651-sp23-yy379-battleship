package edu.duke.yy379.battleship;

import java.util.HashSet;
import java.util.HashMap;

public class ShipZ<T> extends BasicShip<T>{
    private final String name;
    public static int flag = 0;
    public ShipZ(String name, Coordinate upperLeft, char orientation, ShipDisplayInfo<T> myDisplayInfo,
                 ShipDisplayInfo<T> enemyDisplayInfo) {
        super(setAnchor(upperLeft, orientation), makeCoords(upperLeft, orientation), myDisplayInfo, enemyDisplayInfo);
        this.name = name;
    }

    public ShipZ(String name, Coordinate upperLeft, char orientation, T data, T onHit ) {
        this(name, upperLeft, orientation, new SimpleShipDisplayInfo<T>(data, onHit),
                new SimpleShipDisplayInfo<>(null, data));
    }

    /**
     * function to generate coordinates for ShipT
     * @param upperLeft is the upper left coordinate of the ShipT
     * @param coords is the result of coordinates
     * @param direction is the direction of the ShipT (up, down, right, or left)
     */
    static void makeZShip(Coordinate upperLeft, HashSet<Coordinate> coords, String direction) {
        int startRow = upperLeft.getRow() + flag, startCol = upperLeft.getColumn() - flag;
        switch (direction) {
            case "up":
                for (int i = startRow - flag; i < startRow + 4; ++i) {
                    coords.add(new Coordinate(i, startCol));
                }
                for (int i = startRow + 2 - flag; i < startRow + 5; ++i) {
                    coords.add(new Coordinate(i, startCol + 1));
                }
                break;
            case "down":
                for (int i = startRow + flag; i < startRow + 3; ++i) {
                    coords.add(new Coordinate(i, startCol));
                }
                for (int i = startRow + 1; i < startRow + 5; ++i) {
                    coords.add(new Coordinate(i, startCol + 1));
                }
                break;
            case "right":
                for (int j = startCol + 1 -flag; j < startCol + 5; ++j) {
                    coords.add(new Coordinate(startRow, j));
                }
                for (int j = startCol + flag; j < startCol + 3; ++j) {
                    coords.add(new Coordinate(startRow + 1, j));
                }
                break;
            case "left":
                for (int j = startCol + 2; j < startCol + 5; ++j) {
                    coords.add(new Coordinate(startRow, j));
                }
                for (int j = startCol; j < startCol + 4; ++j) {
                    coords.add(new Coordinate(startRow + 1, j));
                }
                break;
            default:
                throw new IllegalArgumentException("Invalid direction: " + direction);
        }
    }

    /**
     * Generate a set of coordinates for a rectangle starting at upperLeft point
     * @param upperLeft is the upper left coordinate of the rectangle
     * @return a HashSet of coordinates
     */
    static HashSet<Coordinate> makeCoords(Coordinate upperLeft, char orientation) {
        HashSet<Coordinate> coords = new HashSet<>();
        switch (orientation) {
            case 'L':
                makeZShip(upperLeft, coords, "left");
                break;
            case 'R':
                makeZShip(upperLeft, coords, "right");
                break;
            case 'D':
                makeZShip(upperLeft, coords, "down");
                break;
            case 'U':
                makeZShip(upperLeft, coords, "up");
                break;
            default:
                throw new IllegalArgumentException("The orientation of T Ship can only be U, L, R, or D");
        }
        return coords;
    }

    static Coordinate setAnchor (Coordinate upperLeft, char orientation) {
        int row = upperLeft.getRow(), col = upperLeft.getColumn();
        int newRow = row, newCol = col;
        if (orientation == 'U') {
            // Do nothing
        } else if (orientation == 'D') {
            newRow = row + 4 + flag;
            newCol = col + 1;
        } else if (orientation == 'L') {
            newRow = row + 1 - flag;
        } else if (orientation == 'R') {
            newCol = col + 4;
        }
        return new Coordinate(newRow, newCol);
    }

    /** return ShipT name
     * @return ship name
     */
    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("ShipZ{name='").append(name).append("', myPieces=").append(myPieces).append("}");
        return builder.toString();
    }

}
