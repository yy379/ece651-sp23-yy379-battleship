package edu.duke.yy379.battleship;

import java.util.Collections;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class BoardTextView {

    private final Board<Character> toDisplay;

    public BoardTextView(Board<Character> toDisplay) {
        this.toDisplay = toDisplay;
    }

    public String displayMyOwnBoard() {
        return displayAnyBoard((c) -> toDisplay.whatIsAtForSelf(c));
    }

    public String displayMyEnemyBoard() {
        return displayAnyBoard((c) -> toDisplay.whatIsAtForEnemy(c));
    }

    protected String displayAnyBoard(Function<Coordinate, Character> getSquareFn) {
        String header = makeHeader();
        StringBuilder ans = new StringBuilder(header);
        for (int row = 0; row < toDisplay.getHeight(); row++){
            char rowIdentifier = (char)('A' + row);
            ans.append(rowIdentifier).append(" ");
            appendRow(ans, row, getSquareFn);
            ans.append(" ").append(rowIdentifier).append("\n");
        }
        ans.append(header);
        return ans.toString();
    }

    protected String makeHeader(){
        // This code displays the string "  " on the screen.

        String header = "  ";
        String sep = "";
        for(int col = 0; col < toDisplay.getWidth(); col ++){
            header += sep + (col + 1);
            sep = " ";
        }
        header += " \n";
        return header;
    }

    /**
     * This makes the header line, e.g. 0|1|2|3|4\n
     *
     * @return the String that is the header line for the given board
     */
    String makeHeader() {
        StringBuilder ans = new StringBuilder("  "); // README shows two spaces at
        String sep=""; //start with nothing to separate, then switch to | to separate
        String columnLabels = makeColumnLabels(sep);
        ans.append(columnLabels);
        ans.append("\n");
        return ans.toString();
    }

    private String makeColumnLabels(String sep) {
        String ans = "";
        for (int column = 0; column < toDisplay.getWidth(); column++) {
            ans = ans + sep + column;
            sep = "|";
        }
        return ans;
    }
    /**
     * This function display two boards side by side
     */
    public String displayMyBoardWithEnemyNextToIt(BoardTextView enemyView, String myHeader, String enemyHeader) {
        String[] myStrings = this.displayMyOwnBoard().split("\n");
        String[] enemyStrings = enemyView.displayMyEnemyBoard().split("\n");

        int width = this.toDisplay.getWidth();
        String header = this.formatHeader(myHeader, enemyHeader, width);

        String result = IntStream.range(0, myStrings.length)
                .mapToObj(i -> {
                    String row = myStrings[i];
                    String enemyRow = enemyStrings[i];
                    String spacing = " ".repeat(2 * width + 19 - row.length());
                    return row + spacing + enemyRow;
                })
                .collect(Collectors.joining("\n"));

        return header + result + "\n";
    }


    private String formatHeader(String myHeader, String enemyHeader, int w) {
        String header = " ".repeat(5) + myHeader;
        header += " ".repeat(2 * w + 22 - header.length()) + enemyHeader + "\n";
        return header;
    }
}

