package edu.duke.yy379.battleship;

public class ShipFactory implements AbstractShipFactory<Character>{
    @Override
    public Ship<Character> makeSubmarine(Placement where){
        return createShip(where, 1, 2, 's', "Submarine");
    }

    @Override
    public Ship<Character> makeBattleship(Placement where){
        return createShip(where, 1, 4, 'b', "Battleship");
    }

    @Override
    public Ship<Character> makeCarrier(Placement where){
        return createShip(where, 1, 6, 'c', "Carrier");
    }

    @Override
    public Ship<Character> makeDestroyer(Placement where){
        return createShip(where, 1, 3, 'd', "Destroyer");
    }

    protected Ship<Character> createShip(Placement where, int w, int h, char letter, String name) {
        int shipWidth = where.getOrientation() == 'V' ? w : h;
        int shipHeight = where.getOrientation() == 'V' ? h : w;
        return new RectangleShip<>(name, where.getWhere(), shipWidth, shipHeight, letter, '*');
    }

}


