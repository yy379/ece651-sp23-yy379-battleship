package edu.duke.yy379.battleship;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public abstract class BasicShip<T> implements Ship<T> {
    protected HashMap<Coordinate, Boolean> myPieces;
    protected ShipDisplayInfo<T> myDisplayInfo;
    protected ShipDisplayInfo<T> enemyDisplayInfo;
    protected Coordinate upper_;//upper left coordinate of the ship

    /**
     * @param where is the Coordinate iterable
     * @param myDisplayInfo is player's display info
     * @param enemyDisplayInfo is enemy's display info
     */
    public BasicShip(Coordinate upper_, Iterable<Coordinate> where, ShipDisplayInfo<T> myDisplayInfo, ShipDisplayInfo<T> enemyDisplayInfo) {
        myPieces = new HashMap<>();
        for (Coordinate c : where) {
            myPieces.put(c, false);
        }
        this.myDisplayInfo = myDisplayInfo;
        this.enemyDisplayInfo = enemyDisplayInfo;
        this.upper_ = upper_;
    }

    /**
     * Check the coordinate is in the ship
     * @param where is the coordinate to check
     * @return true if the coordinate is in the ship
     */
    @Override
    public boolean occupiesCoordinates(Coordinate where) {
        // Iterate through the myPieces HashMap, using an iterator
        Iterator whereIterator = myPieces.entrySet().iterator();
        // Iterate through the HashMap
        while (whereIterator.hasNext()) {
            // Get the next element in the HashMap
            Map.Entry mapElement = (Map.Entry)whereIterator.next();
            // If the where Iterator is equal to the key in the map element, then return true
            if (mapElement.getKey().equals(where)) {
                return true;
            }
        }
        // If the while loop did not find a match, return false
        return false;
    }

    /**
     * Check if the ship is sunk
     * @return true if the ship is sunk (all coordinates are hit)
     */
    @Override
    public boolean isSunk() {
        //Use for-each loop to iterate through the coordinates
        for (Coordinate c: myPieces.keySet()) {
            // If any of the coordinates are not hit, return false
            if (myPieces.get(c) == false) {
                return false;
            }
        }
        // If all the coordinates are hit, return true
        return true;

    }

    @Override
    public Coordinate getUpper_() {
        return upper_;
    }


    /**
     * Get all the Coordinates that this Ship occupies.
     * @return An Iterable with the coordinates that this Ship occupies
     */
    public Iterable<Coordinate> getCoordinates() {
        return myPieces.keySet();
    }



}
