package edu.duke.yy379.battleship;

public class Placement {
    //Define a final Coordinate
    private final Coordinate where;
    //Define a final char orientation
    private final char orientation;

    //Get the coordinate
    public Coordinate getWhere() {
        return where;
    }

    //Get the orientation
    public char getOrientation() {
        return orientation;
    }

    /**
     * This constructor takes a Coordinate and a char
     * to counstruct a Placement
     * @param where the coordiante of upper left part
     * @param orientation orientation of the ship
     * @throws IllegalArgumentException if the orientation is not V' or 'H', 'U', 'L', 'R', 'D' (both upper and lower case)
     */
    public Placement(Coordinate where, char orientation) {
        //check invalid orientation and throw IllegalArgumentException
        //The orientation should be V' or 'H', 'U', 'L', 'R', 'D' (both upper and lower case)
        if (orientation != 'V' && orientation != 'H' && orientation != 'U' && orientation != 'L' && orientation != 'R'
                && orientation != 'D' && orientation != 'v' && orientation != 'h' && orientation != 'u'
                && orientation != 'l' && orientation != 'r' && orientation != 'd') {
            throw new IllegalArgumentException("Invalid orientation: " + orientation);
        }
        this.where = where;
        //convert the orientation to upper case
        this.orientation = Character.toUpperCase(orientation);
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof Placement)) {
            return false;
        }
        Placement p = (Placement) o;
        return p.where.equals(this.where) && p.orientation == this.orientation;
    }
    @Override
    public String toString() {
        return where.toString() + orientation + "";
    }

    @Override
    public int hashCode() {
        return toString().hashCode();
    }

    /**
     * This constructor takes a string description of a placement
     * which is in the format like "A1V" or "b2h"
     * @param descr is the string
     * The first two characters are the coordinate
     * The last character is the orientation :
     * 'V' or 'H', 'U', 'L', 'R', 'D' (both upper and lower case)
     * 'V' means vertical, 'H' means horizontal, 'U' means up, 'L' means left, 'R' means right, 'D' means down
     * @throws IllegalArgumentException if the string is not in the right format
     */
    public Placement(String descr) {
        if (descr.length() != 3) {
            throw new IllegalArgumentException("Invalid placement string: " + descr);
        }
        //check the first character is a letter
        if (!Character.isLetter(descr.charAt(0))) {
            throw new IllegalArgumentException("Invalid placement string: " + descr);
        }
        //check the second character
        if (descr.charAt(1) < '0' || descr.charAt(1) > '9') {
            throw new IllegalArgumentException("Invalid placement string: " + descr);
        }
        //check the third character (both lower case and upper case are valid)
        if (descr.charAt(2) != 'V' && descr.charAt(2) != 'H' && descr.charAt(2) != 'U' && descr.charAt(2) != 'L'
                && descr.charAt(2) != 'R' && descr.charAt(2) != 'D' && descr.charAt(2) != 'v'
                && descr.charAt(2) != 'h' && descr.charAt(2) != 'u' && descr.charAt(2) != 'l'
                && descr.charAt(2) != 'r' && descr.charAt(2) != 'd') {
            throw new IllegalArgumentException("Invalid placement string: " + descr);
        }
        //covert the string to upper case
        descr = descr.toUpperCase();
        //construct the coordinate
        this.where = new Coordinate(descr.charAt(0) - 'A', descr.charAt(1) - '0');
        this.orientation = descr.charAt(2);
    }

    public Coordinate getCoordinate() {
        return where;
    }
}
