package edu.duke.yy379.battleship;

public class OrientationRuleChecker<T> {
    protected final String message = "That placement is invalid: the orientation of %s should be %s!\n";

    /**
     * @param shipName is the ship to be placed
     *                For submarine and destroyer, the orientation should be either 'V' or 'H'
     *                For battleship and carrier, the orientation should be either 'U', 'L', 'R', or 'D'
     * @param orientation is the orientation of the ship
     * @return null if the orientation is valid, otherwise return the error message
     */
    protected String checkMyRule(String shipName, char orientation) {
        //String shipName = theShip.getName();
        String orientationList = getOrientationList(shipName);
        if (!orientationList.contains(String.valueOf(orientation))) {
            return String.format(message, shipName, orientationList);
        }
        return null; //if the orientation is valid, return null
    }

    private String getOrientationList(String shipName) {
        switch (shipName) {
            case "Submarine":
            case "Destroyer":
                return "V or H";
            case "Battleship":
            case "Carrier":
                return "U, L, R, or D";
            default:
                throw new IllegalArgumentException("Invalid ship name: " + shipName);
        }
    }

}


