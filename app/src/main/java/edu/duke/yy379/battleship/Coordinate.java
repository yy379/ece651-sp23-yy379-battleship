package edu.duke.yy379.battleship;

public class Coordinate {
    //define two private final int for row and for column
    private final int row;
    private final int column;
    //getter for row and column
    public int getRow() {
        return row;
    }
    public int getColumn() {
        return column;
    }
    //constructor that takes two int parameters and initialize the row and column
    public Coordinate(int row, int column) {
        this.row = row;
        this.column = column;
    }
    /**
     * Check if two objects are equal.
     */
    @Override
    public boolean equals(Object o) {
        if (o.getClass().equals(getClass())) {
            Coordinate c = (Coordinate) o;
            return row == c.row && column == c.column;
        }
        return false;
    }
    /**
     * Return a hash code for this object.
     */
    @Override
    public int hashCode() {
        return toString().hashCode();
    }
    /**
     * Return a string representation of this object.
     */
    @Override
    public String toString() {
        return "("+row+", " + column+")";
    }
    /**
     * Coordinate must be constructed with a character in range A ~ Z and a number in range 0 ~ 9
     */
    public Coordinate(String descr) {
        //Transfer the string to all upper case
        descr = descr.toUpperCase();
        char rowChar = descr.charAt(0);
        row = rowChar - 'A';
        int col = Integer.parseInt(descr.substring(1));
        column = col;
    }


    }
