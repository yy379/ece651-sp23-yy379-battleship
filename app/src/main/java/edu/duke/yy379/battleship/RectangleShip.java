package edu.duke.yy379.battleship;

import java.util.HashSet;

public class RectangleShip<T> extends BasicShip<T> {

    private final String name;
    /**
     * Get the name of this Ship, such as "submarine".
     * @return the name of this ship
     */


    public RectangleShip(String name, Coordinate upperLeft, int width, int height, ShipDisplayInfo<T> myDisplayInfo,
                         ShipDisplayInfo<T> enemyDisplayInfo) {
        super(upperLeft, makeCoords(upperLeft, width, height), myDisplayInfo, enemyDisplayInfo);
        this.name = name;
    }


    public RectangleShip(String name, Coordinate upperLeft, int width, int height, T data, T onHit) {
        this(name, upperLeft, width, height, new SimpleShipDisplayInfo<T>(data, onHit),
                new SimpleShipDisplayInfo<>(null, data));

    }


    public RectangleShip(Coordinate upperLeft, T data, T onHit) {
        this("testship", upperLeft, 1, 1, data, onHit);
    }

    @Override
    public String getName() {
        return name;
    }

}
