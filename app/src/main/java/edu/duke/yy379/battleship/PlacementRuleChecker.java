package edu.duke.yy379.battleship;

public abstract class PlacementRuleChecker<T> {
    private final PlacementRuleChecker<T> next;
    //more stuff
    public PlacementRuleChecker(PlacementRuleChecker<T> next) {
        this.next = next;
    }
    public String checkPlacement(Ship<T> theShip, Board<T> theBoard) {
        // Iterate over all the rules in the chain
        Rule<T> currentRule = this;
        while (currentRule != null) {
            String message = currentRule.apply(theShip, theBoard);
            if (message != null) {
                return message;
            }
            currentRule = currentRule.getNext();
        }
        return null;
    }

    protected abstract String apply(Ship<T> theShip, Board<T> theBoard);


}

