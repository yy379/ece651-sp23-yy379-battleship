package edu.duke.yy379.battleship;

public interface Board<T> {
    //get width and height
    public int getWidth();
    public int getHeight();

    Iterable<Ship<T>> getShips();

    Ship<T> getShipAt(Coordinate coordinate);

    public String tryAddShip(Ship<T> toAdd);
    public T whatIsAtForSelf(Coordinate where);

    public T whatIsAtForEnemy(Coordinate where);

    public Ship<T> fireAt(Coordinate c);

    public String tryMoveShip(Ship<T> shipToMove, Ship<T> shipAfterMove);

    public void updateMoveShipHitCoords(Ship<T> shipToMove, Ship<T> shipAfterMove);

    T getMissInfo();
    public boolean checkAllSunk();
    String checkPlacementOrientation(String shipName, Placement placement);
}
