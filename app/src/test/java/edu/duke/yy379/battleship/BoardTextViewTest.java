package edu.duke.yy379.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class BoardTextViewTest {

    private final ShipFactory factory = new ShipFactory();
    @Test
    public void test_display_empty_2by2() {
        Board<Character> b1 = new BattleShipBoard<Character>(2, 2, 'X');
        BoardTextView view = new BoardTextView(b1);
        String expectedHeader= "  0|1\n";
        assertEquals(expectedHeader, view.makeHeader());
        String expected=
                expectedHeader+
                        "A  |  A\n"+
                        "B  |  B\n"+
                        expectedHeader;
        assertEquals(expected, view.displayMyOwnBoard());
    }
    @Test
    public void test_invalid_board_size() {
        Board<Character> wideBoard = new BattleShipBoard<Character>(11,20, 'X');
        Board<Character> tallBoard = new BattleShipBoard<Character>(10,27, 'X');
        //you should write two assertThrows here
        assertThrows(IllegalArgumentException.class, () -> new BoardTextView(wideBoard));
        assertThrows(IllegalArgumentException.class, () -> new BoardTextView(tallBoard));
    }
    private void emptyBoardHelper(int w, int h, String expectedHeader, String body) {
        Board<Character> b1 = new BattleShipBoard<Character>(w, h, 'X');
        BoardTextView view = new BoardTextView(b1);
        assertEquals(expectedHeader, view.makeHeader());
        String expected = expectedHeader + body + expectedHeader;
        assertEquals(expected, view.displayMyOwnBoard());
    }
    @Test
    public void test_display_empty_3by2() {
        emptyBoardHelper(3, 2, "  0|1|2\n", "A  | |  A\nB  | |  B\n");
    }
    @Test
    public void test_display_empty_3by5() {
        emptyBoardHelper(3, 5, "  0|1|2\n", "A  | |  A\nB  | |  B\nC  | |  C\nD  | |  D\nE  | |  E\n");
    }
    private void nonEmptyBoardHelper(int w, int h, String expectedHeader, String body){
        Board<Character> b1 = new BattleShipBoard<Character>(w, h, 'X');
        b1.tryAddShip(new RectangleShip(new Coordinate(0, 0), 's', '*'));
        b1.tryAddShip(new RectangleShip(new Coordinate(2, 3), 's', '*'));
        BoardTextView view = new BoardTextView(b1);
        assertEquals(expectedHeader, view.makeHeader());
        String expected = expectedHeader + body + expectedHeader;
        assertEquals(expected, view.displayMyOwnBoard());
    }
    @Test
    public void test_display_not_empty4by3() {
        String expectedHeader = new String("  0|1|2|3\n");
        String expectedBody = new String("A s| | |  A\nB  | | |  B\nC  | | |s C\n");
        nonEmptyBoardHelper(4, 3, expectedHeader, expectedBody);
    }


}

