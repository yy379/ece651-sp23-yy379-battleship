package edu.duke.yy379.battleship;

import org.junit.jupiter.api.Test;

import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.*;

public class RectangleShipTest {

    @Test
    public void test_make_coords() {
        Coordinate c = new Coordinate(10, 20);
        HashSet<Coordinate> test_coord = RectangleShip.makeCoords(c, 1, 3);
        assertEquals(true, test_coord.contains(new Coordinate(10, 20)));
        assertEquals(true, test_coord.contains(new Coordinate(11, 20)));
        assertEquals(true, test_coord.contains(new Coordinate(12, 20)));
        assertEquals(false, test_coord.contains(new Coordinate(13, 20)));
        assertEquals(false, test_coord.contains(new Coordinate(9, 20)));
        assertEquals(false, test_coord.contains(new Coordinate(10, 21)));
        assertEquals(false, test_coord.contains(new Coordinate(11, 21)));
        assertEquals(false, test_coord.contains(new Coordinate(12, 21)));
    }

    @Test
    public void test_RectangleShip() {
        Coordinate c = new Coordinate(5, 5);
        RectangleShip r = new RectangleShip(c, 's', '*');
        assertEquals(true, r.occupiesCoordinates(c));
        assertEquals(false, r.occupiesCoordinates(new Coordinate(5, 6)));
        assertEquals(false, r.occupiesCoordinates(new Coordinate(6, 5)));
    }

    @Test
    public void test_hit() {
        Coordinate c1 = new Coordinate(1, 1);
        Coordinate c2 = new Coordinate(1, 2);
        Coordinate c3 = new Coordinate(2, 2);
        RectangleShip r = new RectangleShip(c1, 's', '*');
        r.recordHitAt(c1);
        assertEquals(true, r.wasHitAt(c1));
        assertEquals(false, r.wasHitAt(c2));
        assertThrows(IllegalArgumentException.class, () -> r.recordHitAt(c3));
    }
    @Test
    public void test_sunk() {
        Coordinate c1 = new Coordinate(5, 6);
        Coordinate c2 = new Coordinate(5, 7);
        Coordinate c3 = new Coordinate(5, 8);
        RectangleShip r = new RectangleShip("testship", c1, 3, 1, 's', '*');
        assertEquals(false, r.isSunk());
        r.recordHitAt(c1);
        assertEquals(false, r.isSunk());
        r.recordHitAt(c2);
        assertEquals(false, r.isSunk());
        r.recordHitAt(c3);
        assertEquals(true, r.isSunk());
    }

    @Test
    public void test_get_name() {
        Coordinate c1 = new Coordinate(10, 20);
        RectangleShip<Character> r = new RectangleShip<>(c1, 's', '*');
        assertEquals(r.getName(), "testship");
    }




}