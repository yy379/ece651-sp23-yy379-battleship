package edu.duke.yy379.battleship;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ShipFactoryTest {

    private void checkShip(Ship<Character> testShip, String expectedName, char expectedLetter, Coordinate... expectedLocs) {
        assertEquals(expectedName, testShip.getName());
        for (Coordinate expectedLoc : expectedLocs) {
            assertTrue(testShip.occupiesCoordinates(expectedLoc));
            assertEquals(expectedLetter, testShip.getDisplayInfoAt(expectedLoc, true));
        }
    }


    @Test
    public void test_create_ship() {
        ShipFactory v = new ShipFactory();
        Ship<Character> ship1 = v.makeSubmarine(new Placement(new Coordinate(5, 5), 'V'));
        Ship<Character> ship2 = v.makeDestroyer(new Placement(new Coordinate(20, 10), 'h'));
        Ship<Character> ship3 = v.makeBattleship(new Placement(new Coordinate(1, 1), 'v'));
        Ship<Character> ship4 = v.makeCarrier(new Placement(new Coordinate(10, 10), 'h'));
        checkShip(ship1, "Submarine", 's', new Coordinate(5, 5), new Coordinate(6, 5));
        checkShip(ship2, "Destroyer", 'd', new Coordinate(20, 10), new Coordinate(20, 11), new Coordinate(20, 12));
        checkShip(ship3, "Battleship", 'b', new Coordinate(1, 1), new Coordinate(2, 1), new Coordinate(3, 1), new Coordinate(4, 1));
        checkShip(ship4, "Carrier", 'c', new Coordinate(10, 10), new Coordinate(10, 11), new Coordinate(10, 12), new Coordinate(10, 13), new Coordinate(10, 14), new Coordinate(10, 15));


    }


}