package edu.duke.yy379.battleship;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
class PlacementTest {

    @Test
    void getWhere() {
        Placement p = new Placement(new Coordinate(1, 2), 'H');
        assertEquals(new Coordinate(1, 2), p.getWhere());
    }

    @Test
    void getOrientation() {
        Placement p = new Placement(new Coordinate(1, 2), 'H');
        assertEquals('H', p.getOrientation());
    }

    @Test
    void testEquals() {
        Placement p1 = new Placement(new Coordinate(1, 2), 'H');
        Placement p2 = new Placement(new Coordinate(1, 2), 'H');
        Placement p3 = new Placement(new Coordinate(1, 3), 'H');
        Placement p4 = new Placement(new Coordinate(3, 2), 'H');
        Placement p5 = new Placement(new Coordinate(1, 2), 'V');
        assertEquals(p1, p1);   //equals should be reflexsive
        assertEquals(p1, p2);   //different objects bu same contents
        assertNotEquals(p1, p3);  //different contents
        assertNotEquals(p1, p4);
        assertNotEquals(p3, p4);
        assertNotEquals(p1, p5);
        assertNotEquals(p1, "(1, 2)"); //different types
    }

    @Test
    void test_string_constructor_valid_cases() {
        Coordinate c1 = new Coordinate("C4");
        assertEquals(2, c1.getRow());
        assertEquals(4, c1.getColumn());
        Coordinate c2 = new Coordinate("D5");
        assertEquals(3, c2.getRow());
        assertEquals(5, c2.getColumn());
        Coordinate c3 = new Coordinate("E8");
        assertEquals(4, c3.getRow());
        assertEquals(8, c3.getColumn());
        Coordinate c4 = new Coordinate("Z0");
        assertEquals(25, c4.getRow());
        assertEquals(0, c4.getColumn());
        Coordinate c5 = new Coordinate("A0");
        assertEquals(0, c5.getRow());
        assertEquals(0, c5.getColumn());
    }
    @Test
    public void test_string_constructor_error_cases() {
        assertThrows(IllegalArgumentException.class, () -> new Coordinate("00"));
        assertThrows(IllegalArgumentException.class, () -> new Coordinate("AA"));
        assertThrows(IllegalArgumentException.class, () -> new Coordinate("@0"));
        assertThrows(IllegalArgumentException.class, () -> new Coordinate("[0"));
        assertThrows(IllegalArgumentException.class, () -> new Coordinate("A/"));
        assertThrows(IllegalArgumentException.class, () -> new Coordinate("A:"));
        assertThrows(IllegalArgumentException.class, () -> new Coordinate("A"));
        assertThrows(IllegalArgumentException.class, () -> new Coordinate("A12"));
    }
}