package edu.duke.yy379.battleship;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BasicShipTest {
    @Test
    public void test_hit() {
        Coordinate upperleft = new Coordinate(2, 3);
        RectangleShip<Character> hit_ship = new RectangleShip<>(upperleft, 's', '*');
        hit_ship.recordHitAt(new Coordinate(2, 3));
        hit_ship.recordHitAt(new Coordinate(2, 4));
        assertTrue(hit_ship.wasHitAt(new Coordinate(2, 3)));
        assertFalse(hit_ship.wasHitAt(new Coordinate(2, 4)));
        assertTrue(hit_ship.wasHitAt(new Coordinate(2, 5)));
        assertFalse(hit_ship.wasHitAt(new Coordinate(2, 6)));
        assertThrows(IllegalArgumentException.class, () -> hit_ship.recordHitAt(new Coordinate(3, 3)));
        assertThrows(IllegalArgumentException.class, () -> hit_ship.wasHitAt(new Coordinate(2, 7)));
    }

    @Test
    public void test_sunk() {
        Coordinate upperleft = new Coordinate(5, 3);
        RectangleShip<Character> sunk_ship = new RectangleShip<>("Destroyer", upperleft, 3, 1, 's', '*');
        assertFalse(sunk_ship.isSunk());
        sunk_ship.recordHitAt(new Coordinate(5, 3));
        assertFalse(sunk_ship.isSunk());
        sunk_ship.recordHitAt(new Coordinate(5, 4));
        assertFalse(sunk_ship.isSunk());
        sunk_ship.recordHitAt(new Coordinate(5, 5));
        assertTrue(sunk_ship.isSunk());
    }

    @Test
    public void test_displayInfo() {
        Coordinate upperleft = new Coordinate(5, 3);
        RectangleShip<Character> test_ship = new RectangleShip<>("Destroyer", upperleft, 3, 1, 's', '*');
        assertEquals(test_ship.getDisplayInfoAt(upperleft, true), 's');
        test_ship.recordHitAt(new Coordinate(5, 3));
        assertEquals(test_ship.getDisplayInfoAt(upperleft, true), '*');
    }

}