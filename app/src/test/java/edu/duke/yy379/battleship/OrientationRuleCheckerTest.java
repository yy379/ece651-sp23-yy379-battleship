package edu.duke.yy379.battleship;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class OrientationRuleCheckerTest {
    private final ShipFactory factory = new ShipFactory();

    @Test
    void test_ship_orientation() {
        OrientationRuleChecker orientationRuleChecker = new OrientationRuleChecker();
        Placement placement = new Placement("b2V");
        Ship<Character> battleship = factory.makeBattleship(placement);
        Ship<Character> submarine = factory.makeSubmarine(placement);
        Ship<Character> carrier = factory.makeCarrier(placement);
        Ship<Character> destroyer = factory.makeDestroyer(placement);

        // correct: test ULRD for battleship & carrier
        assertNull(orientationRuleChecker.checkMyRule("Battleship", 'U'));
        assertNull(orientationRuleChecker.checkMyRule("Battleship", 'L'));
        assertNull(orientationRuleChecker.checkMyRule("Carrier", 'R'));
        assertNull(orientationRuleChecker.checkMyRule("Carrier", 'D'));

        // incorrect: test ULRD for submarine & destroyer
        assertEquals("That placement is invalid: the orientation of Submarine should be V or H!\n", orientationRuleChecker.checkMyRule("Submarine", 'U'));
        assertEquals("That placement is invalid: the orientation of Submarine should be V or H!\n", orientationRuleChecker.checkMyRule("Submarine", 'L'));
        assertEquals("That placement is invalid: the orientation of Destroyer should be V or H!\n", orientationRuleChecker.checkMyRule("Destroyer", 'R'));
        assertEquals("That placement is invalid: the orientation of Destroyer should be V or H!\n", orientationRuleChecker.checkMyRule("Destroyer", 'D'));

        // correct: test VH for submarine & destroyer
        assertNull(orientationRuleChecker.checkMyRule("Submarine", 'V'));
        assertNull(orientationRuleChecker.checkMyRule("Submarine", 'H'));
        assertNull(orientationRuleChecker.checkMyRule("Destroyer", 'V'));
        assertNull(orientationRuleChecker.checkMyRule("Destroyer", 'H'));

        // incorrect: test VH for battleship & carrier
        assertEquals("That placement is invalid: the orientation of Battleship should be U, L, R, or D!\n", orientationRuleChecker.checkMyRule("Battleship", 'V'));
        assertEquals("That placement is invalid: the orientation of Battleship should be U, L, R, or D!\n", orientationRuleChecker.checkMyRule("Battleship", 'H'));
        assertEquals("That placement is invalid: the orientation of Carrier should be U, L, R, or D!\n", orientationRuleChecker.checkMyRule("Carrier", 'V'));
        assertEquals("That placement is invalid: the orientation of Carrier should be U, L, R, or D!\n", orientationRuleChecker.checkMyRule("Carrier", 'H'));
    }
}
