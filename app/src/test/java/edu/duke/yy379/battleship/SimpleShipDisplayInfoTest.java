package edu.duke.yy379.battleship;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SimpleShipDisplayInfoTest {

    @Test
    public void getInfo() {
        SimpleShipDisplayInfo<String> s = new SimpleShipDisplayInfo<>("a", "b");
        assertEquals("a", s.getInfo(new Coordinate(1, 1), false));
        assertEquals("b", s.getInfo(new Coordinate(1, 1), true));
    }

}